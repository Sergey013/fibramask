<?php

add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment($fragments) {
    global $woocommerce;
    ob_start();
    ?>

    <a class="shopping-cart" href="<?php echo esc_url(wc_get_cart_url()); ?>">
        <?php if($woocommerce->cart->cart_contents_count > 0): ?>
            <span><?php echo $woocommerce->cart->cart_contents_count; ?></span>
        <?php endif; ?>
    </a>

    <?php
    $fragments['a.shopping-cart'] = ob_get_clean();
    return $fragments;
}