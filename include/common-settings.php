<?php

add_action('wp_enqueue_scripts', 'common_settings');
function common_settings() {
    wp_enqueue_style('style', get_template_directory_uri().'/dist/assets/css/style.css');
    wp_enqueue_script('javascript', get_template_directory_uri() . '/dist/assets/js/app.js', array(), '20151215', false);
    wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true);
}

add_action('admin_init', 'hide_editor');
function hide_editor() {
    remove_post_type_support('page', 'editor');
    remove_post_type_support('product', 'editor');
}

add_action('add_meta_boxes', 'remove_short_description', 999);
function remove_short_description() {
    remove_meta_box( 'postexcerpt', 'product', 'normal');

}