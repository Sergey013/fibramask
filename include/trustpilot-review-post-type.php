<?php

add_action('init', 'create_post_type_trustpilot_review');
if (!function_exists('create_post_type_trustpilot_review')) {
    function create_post_type_trustpilot_review() {
        $label_singular = esc_html__('Trustpilot review', 'fibramask');

        $args = array(
            'labels' => array(
                'name' => esc_html__('Trustpilot review', 'fibramask'),
                'singular_name' => $label_singular,
                'menu_name' => esc_html__('Trustpilot review', 'fibramask'),
                'parent_item_colon' => ''
            ),
            'show_in_menu' => true,
            'public' => true,
            'has_archive' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => array('slug' => 'trustpilot_review'),
            'query_var' => true,
            'menu_icon' => 'dashicons-star-filled',
            'supports' => array('title', 'editor', 'thumbnail')
        );
        register_post_type('trustpilot_review', $args);
    }
}