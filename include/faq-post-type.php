<?php

add_action('init', 'create_post_type_faq');
if (!function_exists('create_post_type_faq')) {
    function create_post_type_faq() {
        $label_singular = esc_html__('FAQ', 'fibramask');

        $args = array(
            'labels' => array(
                'name' => esc_html__('FAQ', 'fibramask'),
                'singular_name' => $label_singular,
                'menu_name' => esc_html__('FAQ', 'fibramask'),
                'parent_item_colon' => ''
            ),
            'show_in_menu' => true,
            'public' => true,
            'has_archive' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => array('slug' => 'faq'),
            'query_var' => true,
            'menu_icon' => 'dashicons-clipboard',
            'supports' => array('title', 'editor')
        );
        register_post_type('faq', $args);
    }
}