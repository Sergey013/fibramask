<?php

add_action('woocommerce_before_calculate_totals', 'add_custom_price', 20, 1);
function add_custom_price($cart) {
    global $woocommerce;

    $total = 0;

    foreach ($cart->get_cart() as $item) {
        if ($item['quantity'] > 2) {
            $item['data']->set_price(200);
        }

        $rules = get_field('rules', $item['product_id']);
        $total += $item['quantity'];
        foreach ($rules as $rule) {
            if($item['quantity'] >= $rule['quantity']) {
                $item['data']->set_price($rule['piece_price']);
            }
        }
    }

    if ($total >= 3) {
        $woocommerce->session->set('chosen_shipping_methods', array('free_shipping:11'));
    }else{
        //$woocommerce->session->set('chosen_shipping_methods', array('flat_rate:8'));
    }
}