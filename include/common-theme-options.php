<?php

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'Social icons',
        'menu_title' => 'Social icons',
        'menu_slug' => 'theme-options',
        'capability' => 'manage_options',
        'position' => 61.1,
        'redirect' => true,
        'icon_url' => 'dashicons-share',
        'update_button' => 'Save options',
        'updated_message' => 'Options saved',
    ));
}
