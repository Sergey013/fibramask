<?php
add_action('woocommerce_admin_order_data_after_order_details', 'misha_editable_order_meta_general');
function misha_editable_order_meta_general($order) { ?>
    <?php $order_tracking = get_post_meta($order->get_id(), 'order_tracking', true); ?>

    <div class="edit_address" style="display: block">
        <?php
        woocommerce_wp_select(array(
            'id' => 'order_tracking',
            'label' => 'Tracking status:',
            'value' => $order_tracking,
            'options' => array(
                '' => 'Processing',
                'shipping' => 'Shipping',
                'delivered' => 'Delivered'
            ),
            'wrapper_class' => 'form-field-wide'
        ));
        ?>
    </div>
<?php }

add_action('woocommerce_process_shop_order_meta', 'misha_save_general_details');
function misha_save_general_details($order_id){
    update_post_meta($order_id, 'order_tracking', wc_clean( $_POST['order_tracking']));
}