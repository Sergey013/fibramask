<?php
add_action('woocommerce_add_to_cart', 'added_to_cart');

function added_to_cart () {
    $total = 0;
    $rules = get_field('rules', $_POST["product_id"]);

    foreach ($rules as $rule) {
        if($_POST['quantity'] >= $rule['quantity']) {
            $total = strip_tags(wc_price($_POST['quantity'] * $rule['piece_price']));
        }
    }

    $variation = wc_get_product($_POST['variation_id']);
    $updated_image_id = $variation->get_image_id();

    $html = '<div class="modal">';
    $html .= '<div class="modal-wrapper">';
    $html .= '<h3>Added to cart</h3>';
    $html .= '<div class="product-block">';
    $html .= '<div class="product-picture" style="background: rgba(37, 37, 37, 0.03) url('.wp_get_attachment_image_url($updated_image_id).') center no-repeat">';
    $html .= '</div>';
    $html .= '<div class="product-info">';
    $html .= '<h5>'.wc_get_product($_POST["product_id"])->get_title().'</h5>';
    $html .= '<span>Everyday Mask</span>';
    $html .= '<div class="info-block">';
    $html .= '<span class="gray">Color</span>';
    $html .= '<span>'.$_POST["attribute_color"].'</span>';
    $html .= '</div>';
    $html .= '<div class="info-block">';
    $html .= '<span class="gray">Size</span>';
    $html .= '<span>'.$_POST["attribute_size"].'</span>';
    $html .= '</div>';
    $html .= '<div class="info-block">';
    $html .= '<span class="gray">Quantity</span>';
    $html .= '<span>'.$_POST["quantity"].'</span>';
    $html .= '</div>';
    $html .= '<div class="info-block-total">';
    $html .= '<h5>Total</h5>';
    $html .= '<h5>'.$total.'</h5>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="button-container">';

    if (stristr($_SERVER['HTTP_USER_AGENT'], 'android')) {
        $html .= '<a href="#" class="btn google"></a>';
        $html .= '<div id="wc-stripe-payment-request-wrapper" style="clear:both;padding-top:1.5em;display:none;">';
        $html .= '<div id="wc-stripe-payment-request-button">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<hr>';
        $html .= '<a href="'.esc_url(wc_get_cart_url()).'" class="view_cart">View cart</a>';
    }elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'iphone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iphone') || stristr($_SERVER['HTTP_USER_AGENT'], 'ipad')) {
        $html .= '<a href="#" class="btn apple"></a>';
        $html .= '<div id="wc-stripe-payment-request-wrapper" style="clear:both;padding-top:1.5em;display:none;">';
        $html .= '<div id="wc-stripe-payment-request-button">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<hr>';
        $html .= '<a href="'.esc_url(wc_get_cart_url()).'" class="view_cart">View cart</a>';
    }else{
        $html .= '<div id="wc-stripe-payment-request-wrapper" style="clear:both;padding-top:1.5em;display:none;">';
        $html .= '<div id="wc-stripe-payment-request-button">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<hr>';
        $html .= '<a href="'.esc_url(wc_get_cart_url()).'" class="view_cart primary">View cart</a>';
    }

    $html .= '<a href="" class="keep_shopping">Keep shopping</a>';
    $html .= '</div>';
    $html .= '</div>';

    echo $html;
};