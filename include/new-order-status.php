<?php

add_action( 'init', 'register_my_new_order_statuses' );

function register_my_new_order_statuses() {
	register_post_status( 'wc-requested-refund', array(
		'label'                     => _x( 'Requested refund', 'Order status', 'woocommerce' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Requested refund', 'Requested refund', 'woocommerce' )
	) );

    register_post_status( 'wc-confirmed-refund', array(
        'label'                     => _x( 'Confirmed refund', 'Order status', 'woocommerce' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Confirmed refund', 'Confirmed refund', 'woocommerce' )
    ) );
}

add_filter( 'wc_order_statuses', 'my_new_wc_order_statuses' );

function my_new_wc_order_statuses( $order_statuses ) {
	$order_statuses['wc-requested-refund'] = _x( 'Requested refund', 'Order status', 'woocommerce' );
	$order_statuses['wc-confirmed-refund'] = _x( 'Confirmed refund', 'Order status', 'woocommerce' );

	return $order_statuses;
}

add_action( 'admin_head', 'requested_label_style' );

function requested_label_style() {
	echo '<style>
			.order-status.status-requested-refund{
				background: #E64A19;
				color:#FFCCBC;
			}
			.order-status.status-confirmed-refund{
				background: #A2D6F2;
			}
  </style>';
}