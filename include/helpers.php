<?php
if (!function_exists('entry_footer')) {
    function entry_footer() {
        ob_start(); ?>
            <footer class="entry-footer">
                <div class="site-wrapper">
                    <?php
                    edit_post_link(
                        sprintf(
                            wp_kses(
                                __( 'Edit <span class="screen-reader-text">%s</span>', 'fibramask' ),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            wp_kses_post( get_the_title() )
                        ),
                        '<span class="edit-link">',
                        '</span>'
                    );
                    ?>
                </div>
            </footer>
        <?php return ob_get_clean();
    }
}