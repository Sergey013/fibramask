<?php

register_nav_menus(array(
    'menu-1' => esc_html__('Primary', 'fibramask'),
    'menu-2' => esc_html__('Footer', 'fibramask'),
    'menu-3' => esc_html__('Mobile', 'fibramask'),
));