<?php

add_action('template_redirect', 'custom_shop_page_redirect');
function custom_shop_page_redirect() {
    if(is_shop()){
        wp_redirect(home_url( '/product/fibramask/'));
        exit();
    }
}