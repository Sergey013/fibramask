<?php

add_action('woocommerce_after_shipping_rate', 'action_after_shipping_rate', 20, 2);
function action_after_shipping_rate ($method, $index) {
    if('flat_rate:8' === $method->id) {
        echo __("<p>Normally delivered within 5-7 days</p>");
    }
    if('flat_rate:9' === $method->id) {
        echo __("<p>Delivered on 24 April 2020 or before</p>");
    }
    if('flat_rate:10' === $method->id) {
        echo __("<p>Delivered tomorrow before 8pm</p>");
    }
}