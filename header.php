<?php
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.typekit.net/nna5ovs.css">
    <title><?php echo get_bloginfo(); ?></title>
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/favicon.png"">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<header class="<?php if(is_front_page()): echo 'mobile-front-header'; endif; ?>">
        <div class="site-wrapper">
            <a href="#" class="mobile-icon <?php if(is_front_page()): echo 'white'; endif; ?>"></a>
            <a href="<?php echo home_url(); ?>" class="desktop-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/logo.svg">
            </a>
            <a href="<?php echo home_url(); ?>" class="mobile-logo <?php if(is_front_page()): echo 'mobile-logo-front'; endif; ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/<?php if(is_front_page()): echo 'logo-mobile-white.png'; else: echo 'logo-mobile.svg'; endif; ?>">
            </a>

            <nav>
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    ));
                ?>
                <div class="mobile-menu">
                    <div class="mobile-menu-wrapper">
                        <a href="#" class="close-btn"><img src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/close.svg"></a>
                        <?php
                            wp_nav_menu(array(
                                'theme_location' => 'menu-3',
                                'menu_id'        => 'mobile-menu',
                            ));
                        ?>
                    </div>
                </div>
            </nav>
            <div class="<?php if(is_front_page()): echo 'mobile-front'; endif; ?>">
            <a class="shopping-cart" href="<?php echo wc_get_cart_url(); ?>">
                <?php if(WC()->cart->get_cart_contents_count() > 0): ?>
                    <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                <?php endif;?>
            </a>
            </div>
        </div>
	</header>
    <span class="subtext <?php if(is_front_page()): echo 'mobile-front-subtext'; endif; ?>">The Original</span>
	<div id="content" class="site-content">