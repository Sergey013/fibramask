<div class="site-wrapper">
    <div class="hero-block">
        <div class="picture">
            <img src="<?php echo $image; ?>" alt="" class="desktop">
            <img src="<?php echo $mobile_image; ?>" alt="" class="mobile">
            <?php if($ready_to_get[0] == 'yes'): ?>
                <div class="ready-to-order">
                    <h3><?php echo $title; ?></h3>
                    <span><?php echo $subtitle; ?></span>
                    <div class="colors">
                        <?php foreach ($colors as $color): ?>
                            <span style="background: <?php echo $color['color']; ?>"></span>
                        <?php endforeach; ?>
                    </div>
                    <a href="<?php echo get_permalink($attach_product); ?>" class="btn"><?php echo $button_name; ?></a>
                    <div class="continue"></div>
                </div>
            <?php endif; ?>
        </div>
        <div class="content">
            <div class="stars">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <h2><?php echo wpautop($text); ?></h2>

            <?php if($show_advantages[0] == 'yes'): ?>
                <div class="advantages">
                    <?php foreach ($advantages as $advantage): ?>
                        <div class="advantage-item">
                            <div class="icon">
                                <img src="<?php echo $advantage['icon']; ?>" alt="">
                            </div>
                            <h3><?php echo $advantage['title']; ?></h3>
                            <span><?php echo $advantage['text']; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>