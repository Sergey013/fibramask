<div class="page-wrapper">
    <div class="ready-to-get">
        <div class="picture">
            <img src="<?php echo $image; ?>" alt="">
        </div>
        <div class="content">
            <h3><?php echo $title; ?></h3>
            <span><?php echo $subtitle; ?></span>
            <div class="colors">
                <?php foreach ($colors as $color): ?>
                    <span style="background: <?php echo $color['color']; ?>"></span>
                <?php endforeach; ?>
            </div>
            <a href="<?php echo get_permalink($attach_product); ?>" class="btn"><?php echo $button_name; ?></a>
        </div>
    </div>
</div>