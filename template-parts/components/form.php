<?php
    $form_title = get_post($form)->post_title;
?>

<div class="form-block">
    <div class="page-wrapper">
        <h2><?php echo $form_title; ?></h2>
        <?php echo do_shortcode('[contact-form-7 id="'.$form.'" title="'.$form_title.'"]'); ?>
    </div>
</div>
