<div class="support-block">
    <div class="page-wrapper">
        <h2><?php echo $title; ?></h2>
        <p><?php echo wpautop($subtitle); ?></p>

        <div class="support-items">
            <?php foreach ($items as $item): ?>
                <a href="<?php echo get_permalink($item['page']); ?>" class="item">
                    <div class="item-icon">
                        <img src="<?php echo $item['icon']; ?>" alt="">
                    </div>
                    <h4><?php echo $item['title']; ?></h4>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>