<div class="text-block">
	<?php if ( ! empty( $title ) ): ?>
        <h2><?php echo $title; ?></h2>
	<?php endif; ?>
	<?php if ( ! empty( $text ) ): ?>
        <p><?php echo wpautop( $text ); ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $image ) ): ?>
        <div class="image-container four-image">
            <img src="<?php echo $image; ?>" alt=""
                 style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
			<?php if ( ! empty( $second_image ) ): ?>
                <img src="<?php echo $second_image; ?>" alt=""
                     style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
			<?php endif; ?>

	        <?php if ( ! empty( $third_image ) ): ?>
                <img src="<?php echo $third_image; ?>" alt=""
                     style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
	        <?php endif; ?>

	        <?php if ( ! empty( $fourth_image ) ): ?>
                <img src="<?php echo $fourth_image; ?>" alt=""
                     style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
	        <?php endif; ?>
        </div>
	<?php endif; ?>
</div>