<div class="trustpilot-block">
    <div class="trustpilot-wrapper">
        <h3><?php echo $title; ?></h3>

        <div class="rate">
            <?php foreach (range(1, $rate) as $star): ?>
                <span></span>
            <?php endforeach; ?>
        </div>

        <span class="review">TrustScore: <strong><?php echo $score; ?></strong> | <u><?php echo $total_reviews; ?> reviews</u></span>

        <div class="reviews-block">
            <?php foreach ($reviews as $review): ?>
            <div class="review-item">
                <div class="picture" style="background: url(<?php echo get_the_post_thumbnail_url($review->ID); ?>)"></div>
                <div class="content">
                    <?php echo wpautop($review->post_content); ?>
                    <strong><?php echo $review->post_title; ?></strong>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>