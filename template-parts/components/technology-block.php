<div class="page-wrapper">
    <div class="technology-block">
        <div class="content">
            <h3><?php echo $title; ?></h3>
            <span><?php echo wpautop($text); ?></span>

            <?php echo wpautop($list); ?>
        </div>
        <div class="picture">
            <img src="<?php echo $image; ?>" alt="" class="desktop">
            <img src="<?php echo $mobile_image; ?>" alt="" class="mobile">
        </div>
        <a href="<?php echo get_permalink($attach_product); ?>" class="btn"><?php echo $button_name; ?></a>
    </div>
</div>