<div class="text-block">
	<?php if ( ! empty( $title ) ): ?>
        <h2><?php echo $title; ?></h2>
	<?php endif; ?>
	<?php if ( ! empty( $text ) ): ?>
        <p><?php echo wpautop( $text ); ?></p>
	<?php endif; ?>
	<?php if ( ! empty( $image ) ): ?>
        <div class="image-container <?php if ( !empty( $second_image ) && !empty( $image ) ) {
			echo ' two-images';
        } ?>">
            <!--            <div style="background: #c4c4c4 url(<?php //echo $image; ?>/*/*) center no-repeat; */ -->
            <!--         */<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0;'; endif; ?> <?php if ( empty( $second_image ) ): echo 'margin-right: 0;'; endif; ?>"></div>-->

            <img src="<?php echo $image; ?>" alt=""
                 style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
			<?php if ( ! empty( $second_image ) ): ?>
                <!--                <div style="background: #c4c4c4 url(<?php //echo $second_image; ?>/*) center no-repeat; -->
                <!--    */<?php //if(empty($text) && empty($title)): echo 'margin: 0'; endif; ?><!--"></div>-->
                <img src="<?php echo $second_image; ?>" alt=""
                     style="<?php if ( empty( $text ) && empty( $title ) ): echo 'margin: 0'; endif; ?>">
			<?php endif; ?>
        </div>
	<?php endif; ?>
	<?php if ( ! empty( $button_link ) && ! empty( $button_title ) ): ?>
        <a href="<?php echo site_url() . $button_link ?>" class="btn"><?php echo $button_title; ?></a>
	<?php endif; ?>
</div>