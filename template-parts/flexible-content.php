<?php if(have_rows('content')):?>
    <?php while (have_rows('content')): the_row(); ?>
        <?php if(get_row_layout() == 'text_block'): ?>
            <?php
                set_query_var('title', get_sub_field('title'));
                set_query_var('text', get_sub_field('text'));
                set_query_var('image', get_sub_field('image'));
                set_query_var('second_image', get_sub_field('second_image'));
                set_query_var('button_title', get_sub_field('button_title'));
                set_query_var('button_link', get_sub_field('button_link'));
            ?>
            <?php get_template_part('template-parts/components/text-block'); ?>
        <?php endif; ?>

		<?php if(get_row_layout() == 'four_image'): ?>
			<?php
			set_query_var('title', get_sub_field('title'));
			set_query_var('text', get_sub_field('text'));
			set_query_var('image', get_sub_field('image'));
			set_query_var('second_image', get_sub_field('second_image'));
			set_query_var('third_image', get_sub_field('third_image'));
			set_query_var('fourth_image', get_sub_field('fourth_image'));
			?>
			<?php get_template_part('template-parts/components/four-image'); ?>
		<?php endif; ?>


		<?php if(get_row_layout() == 'technology_block'): ?>
            <?php
                set_query_var('title', get_sub_field('title'));
                set_query_var('text', get_sub_field('text'));
                set_query_var('list', get_sub_field('technology_list'));
                set_query_var('image', get_sub_field('image'));
                set_query_var('mobile_image', get_sub_field('mobile_image'));
                set_query_var('button_name', get_sub_field('button_name'));
                set_query_var('attach_product', get_sub_field('attach_product'));
            ?>
            <?php get_template_part('template-parts/components/technology-block'); ?>
        <?php endif; ?>

		<?php if(get_row_layout() == 'everyday-life'): ?>
			<?php
			set_query_var('title', get_sub_field('title'));
			set_query_var('text', get_sub_field('text'));
			set_query_var('list', get_sub_field('technology_list'));
			set_query_var('image', get_sub_field('image'));
			set_query_var('mobile_image', get_sub_field('mobile_image'));
			set_query_var('button_name', get_sub_field('button_name'));
			set_query_var('attach_product', get_sub_field('attach_product'));
			?>
			<?php get_template_part('template-parts/components/everyday-life'); ?>
		<?php endif; ?>

        <?php if(get_row_layout() == 'support'): ?>
            <?php
                set_query_var('title', get_sub_field('title'));
                set_query_var('subtitle', get_sub_field('subtitle'));
                set_query_var('items', get_sub_field('support_items'));
            ?>
            <?php get_template_part('template-parts/components/support'); ?>
        <?php endif; ?>

        <?php if(get_row_layout() == 'trustpilot'): ?>
            <?php
                set_query_var('title', get_sub_field('title'));
                set_query_var('rate', get_sub_field('rate'));
                set_query_var('score', get_sub_field('score'));
                set_query_var('total_reviews', get_sub_field('total_reviews'));
                set_query_var('reviews', get_sub_field('reviews'));
            ?>
            <?php get_template_part('template-parts/components/trustpilot'); ?>
        <?php endif; ?>

        <?php if(get_row_layout() == 'hero_section'): ?>
            <?php
                set_query_var('text', get_sub_field('text'));
                set_query_var('image', get_sub_field('image'));
                set_query_var('mobile_image', get_sub_field('mobile_image'));
                set_query_var('ready_to_get', get_sub_field('show_ready_to_get'));
                set_query_var('title', get_sub_field('title'));
                set_query_var('subtitle', get_sub_field('subtitle'));
                set_query_var('colors', get_sub_field('colors'));
                set_query_var('button_name', get_sub_field('button_name'));
                set_query_var('attach_product', get_sub_field('attach_product'));
                set_query_var('show_advantages', get_sub_field('show_advantages'));
                set_query_var('advantages', get_sub_field('advantages'));
            ?>
            <?php get_template_part('template-parts/components/hero'); ?>
        <?php endif; ?>

        <?php if(get_row_layout() == 'ready_to_get'): ?>
            <?php
                set_query_var('image', get_sub_field('image'));
                set_query_var('title', get_sub_field('title'));
                set_query_var('subtitle', get_sub_field('subtitle'));
                set_query_var('colors', get_sub_field('colors'));
                set_query_var('button_name', get_sub_field('button_name'));
                set_query_var('attach_product', get_sub_field('attach_product'));
            ?>
            <?php get_template_part('template-parts/components/ready-to-get'); ?>
        <?php endif; ?>

        <?php if(get_row_layout() == 'form'): ?>
            <?php
                set_query_var('form', get_sub_field('form_select'));
            ?>
            <?php get_template_part('template-parts/components/form'); ?>
        <?php endif; ?>

        <?php if(get_row_layout() == 'shortcode'): ?>
            <?php
            set_query_var('shortcode', get_sub_field('do_shortcode'));
            ?>
            <?php get_template_part('template-parts/components/do-shortcode'); ?>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>
