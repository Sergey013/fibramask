const AddedToCartPopup = jQuery(document).ready(function($) {
    if($('.modal').length <= 0) return;

    $('.modal').fadeIn();

    $('.modal').on('click', '.keep_shopping', function (e) {
        e.preventDefault();
        $('.modal').fadeOut('slow', function() {
            $('.modal').remove();
        });
    });

    $('.modal').click(function(e) {
        $(this).fadeOut('slow', function() {
            $('.modal').remove();
        });
    });

    $('.modal-wrapper').click(function(e) {
        e.stopPropagation();
    });
});

module.exports = AddedToCartPopup;