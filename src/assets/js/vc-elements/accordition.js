const Accordition = jQuery(document).ready(function($) {
    $(".accordition-block").each(function() {
        $(this).find(".toggle").click(function(e) {
            $(this).off("mouseenter mouseleave")
            e.preventDefault()

            if($(this).find("div").hasClass("show")) {
                $(this)
                    .removeClass('opened')
                    .removeClass('active')
                    .find("div")
                    .removeClass('show')
                    .slideUp(350)
            }else {
                $(this)
                    .addClass('opened')
                    .find("div")
                    .addClass('show')
                    .slideDown(350)
            }
        })

    });

    let interval = setInterval(function () {
        $('.flex-viewport figure div img').each(function () {
            let image = $(this).attr('src')
            // $(this).attr('srcset', '')
            // $(this).attr('src', '')

            $(this).hide()
            $(this).parent().css({'width': '100%', 'height': '100%', 'background': '#f8f8f8 url(' + image + ') no-repeat'})
        })

        clearInterval(interval)
    }, 300)

    let zoomInterval = setInterval(function () {
        if ($('.zoomImg').length > 0) {
            $('.zoomImg').remove()
        }
    }, 50)

});

module.exports = Accordition;