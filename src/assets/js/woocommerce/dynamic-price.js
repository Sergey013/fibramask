const DynamicPrice = jQuery(document).ready(function($) {
    $('.dynamic-price').on('click', '.select-items div', function (e) {
        e.preventDefault();
        let total = $(this).attr('quantity') * $(this).attr('price')

        $('.woocommerce-Price-amount').html('<span class="woocommerce-Price-currencySymbol">€</span>' + total.toFixed(2) + '')
    })
});

module.exports = DynamicPrice;