const VariationGallery = jQuery(document).ready(function ($) {
	var interval;

	if ($('.color-variation').length <= 0) return;

	var orig = $.fn.css;
	$.fn.css = function () {
		var result = orig.apply(this, arguments);
		$(this).trigger('stylechanged');
		return result;
	}

	$('.single_add_to_cart_button').click(function (e) {
		$("#color").change();
		$("#size").change();
	})

	interval = setInterval(function (e) {
		let color
		$('.woocommerce-product-gallery__wrapper').on("stylechanged", function (e) {
			let val = $('.woocommerce-product-gallery__wrapper').attr('style').split(';')[2].replace('transform: translate3d(', '').replace('px, 0px, 0px)', '')

			if ($(document).width() > 600) return


			// if (parseInt(val) < 0 && parseInt(val) > -1600) {
			if (parseInt(val) <= 0 && parseInt(val) > -2000) {
				// color = 'grey';
				color = 'black';
			}

			// if (parseInt(val) <= -1600 && parseInt(val) > -3200) {
			if (parseInt(val) <= -5600) {
				color = 'blue'
				// color = 'gray'

			}

			// if (parseInt(val) <= -3200 && parseInt(val) > -4800) {
			if (parseInt(val) >= -3600 && parseInt(val) <= -2400) {
				color = 'grey'
				// color = 'pink'
			}

			// if (parseInt(val) <= -4800) {
			if (parseInt(val) <= -4000 && parseInt(val) >= -5200) {
				color = 'pink'
				// color = 'blue'
			}

			$('.color-variation span').each(function () {
				$(this).removeClass('selected');

				if ($(this).data('variant') == color) {
					$(this).addClass('selected');
					$("#color").val($(this).data('color'));
				}

				if ($(this).attr("class").indexOf('selected') >= 0) {
					interval = setInterval(function () {
						$('.flex-control-nav img').each(function () {
							if ($(this).attr('src').indexOf(color) < 0) {
								$(this).parent().hide();
							} else {
								$(this).parent().show();
							}
						})
						clearInterval(interval)
					}, 100)
				}
			})
		})

		clearInterval(interval)
	}, 2000);

	$('.woocommerce-product-gallery__wrapper').on('stylechanged', function () {
		$('.flex-control-nav img').each(function () {
			if ($(this).attr('class') != undefined) {
				if ($(this).attr('class').indexOf('flex-active') < 0) {
					$(this).parent().removeClass('active')
				} else {
					$(this).parent().addClass('active')
				}
			}
		})
	});

	$('.color-variation span').each(function () {
		if ($(this).attr("class").indexOf('selected') >= 0) {
			let color = $(this).data("variant");
			interval = setInterval(function () {
				$('.flex-control-nav img').each(function () {
					if ($(this).attr('class') != undefined) {
						if ($(this).attr('class').indexOf('flex-active') < 0) {
							$(this).parent().removeClass('active')
						} else {
							$(this).parent().addClass('active')
						}
					}

					if ($(this).attr('src').indexOf(color) < 0) {
						$(this).parent().hide();
					} else {
						$(this).parent().show();
					}
				})

				clearInterval(interval)
			}, 100)
		}
	})

	$('.color-variation span').on('click', function (e) {
		e.preventDefault();

		$('.color-variation span').each(function () {
			if ($(this).attr("class").indexOf('selected') >= 0) {
				let color = $(this).data("variant");

				interval = setInterval(function () {
					$('.flex-control-nav img').each(function () {
						if ($(this).attr('src').indexOf(color) < 0) {
							$(this).parent().hide();
						} else {
							$(this).parent().show();
						}
					})
					clearInterval(interval)
				}, 100)
			}
		})
	})


});

module.exports = VariationGallery;