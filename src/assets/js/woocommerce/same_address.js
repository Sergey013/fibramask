const SameAddress = jQuery(document).ready(function($) {
    $(document).ajaxComplete(function(e) {
        $('#ship-to-different-address').on('click ', function (e) {
            if ($('#ship-to-different-address-checkbox').prop('checked')) {
                $('.shipping_address').addClass('checked');
                $('.shipping_address').removeClass('non-checked');

                $('#shipping_first_name').val($('#billing_first_name').val())
                $('#shipping_last_name').val($('#billing_last_name').val())
                $('#shipping_address_1').val($('#billing_address_1').val())
                $('#shipping_address_2').val($('#billing_address_2').val())
                $('#shipping_postcode').val($('#billing_postcode').val())
                // alert($('#billing_city').val())
                $('#shipping_city').val($('#billing_city').val())
                $('#select2-shipping_country-container').html($('#select2-billing_country-container').html())
            } else {
                $('.shipping_address').addClass('non-checked');
                $('.shipping_address').removeClass('checked');
                $('.shipping_address input').val('');
                let interval = setInterval(function () {
                    $('#select2-shipping_country-container').html('')
                    clearInterval(interval)
                }, 100)
            }
        })

        let interval_check = setInterval(function () {
            if ($('#ship-to-different-address-checkbox').prop('checked')) {
                $('.shipping_address').addClass('checked');
                $('.shipping_address').removeClass('non-checked');

                $('#shipping_first_name').val($('#billing_first_name').val())
                $('#shipping_last_name').val($('#billing_last_name').val())
                $('#shipping_address_1').val($('#billing_address_1').val())
                $('#shipping_address_2').val($('#billing_address_2').val())
                $('#shipping_postcode').val($('#billing_postcode').val())
                $('#shipping_city').val($('#billing_city').val())
                $('#select2-shipping_country-container').html($('#select2-billing_country-container').html())
            } else {
                $('.shipping_address').addClass('non-checked');
                $('.shipping_address').removeClass('checked');
                $('.shipping_address input').val('');
                let interval = setInterval(function () {
                    $('#select2-shipping_country-container').html('')
                    clearInterval(interval)
                }, 100)
            }
            clearInterval(interval_check)
        }, 200)

        if (!$('#ship-to-different-address-checkbox').prop('checked')) {
            $('.shipping_address').addClass('non-checked');
            $('.shipping_address').removeClass('checked');
            $('.shipping_address input').val('');
            let interval = setInterval(function () {
                $('#select2-shipping_country-container').html('')
                clearInterval(interval)
            }, 100)

        }
    })
});

module.exports = SameAddress;