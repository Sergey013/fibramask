const ShippingUpdate = jQuery(document).ready(function($) {
    $(document).ajaxComplete(function(e) {
        $('.shipping-select .select-selected').remove();
        $('.shipping-select .select-items').remove();

        var x, i, j, selElmnt, a, b, c;
        /* Look for any elements with the class "custom-select": */
        $('.shipping-select').css({'display': 'block', 'position': 'relative'});
        x = document.getElementsByClassName("shipping-select");

        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /* For each element, create a new DIV that will act as the selected item: */
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /* For each element, create a new DIV that will contain the option list: */
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < selElmnt.length; j++) {
                /* For each option in the original select element,
                create a new DIV that will act as an option item: */
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.setAttribute('data-rate', selElmnt.options[j].getAttribute('value'));
                c.addEventListener("click", function(e) {
                    /* When an item is clicked, update the original select box,
                    and the selected item: */
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                /* When the select box is clicked, close any other select boxes,
                and open/close the current select box: */
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {
            /* A function that will close all select boxes in the document,
            except the current select box: */
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");


            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }

        /* If the user clicks anywhere outside the select box,
        then close all select boxes: */
        document.addEventListener("click", closeAllSelect);

        Array.from($('#shipping_method li input')).forEach(function(el) {
            if($(el).attr('checked') == 'checked') {
                $('.select-selected').html($(el).next().text())
            }
        })

        Array.from($('.shipping-select .select-items div')).forEach(function(el) {
            if ($(el).data('rate') == 'free_shipping:11') {
                $(el).hide()
            }
        })


        $('#shipping-select .select-items div').on('click', function (e) {
            let rate = $(this).data('rate');
            Array.from($('#shipping_method li input')).forEach(function(el) {
                if($(el).val() == rate) {
                    $(el).click();
                    $(el).change();
                }
            })
        })
    })
});

module.exports = ShippingUpdate;