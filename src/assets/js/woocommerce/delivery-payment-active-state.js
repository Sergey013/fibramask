const ActiveState = jQuery(document).ready(function ($) {
	// if($('.woocommerce-product-gallery ol.flex-control-nav').length > 0) {


	let interval = setInterval(function () {
		$('.woocommerce-product-gallery ol.flex-control-nav li').each(function () {
			if ($(this).children('img').hasClass('flex-active')) {
				$(this).addClass('flex-active');
			} else {
				$(this).removeClass('flex-active');
			}
		});

		$('.woocommerce-product-gallery').bind('after', function (event, slider) {
			$('.woocommerce-product-gallery ol.flex-control-nav li').each(function () {
				if ($(this).children('img').hasClass('flex-active')) {
					$(this).addClass('flex-active');
				} else {
					$(this).removeClass('flex-active');
				}
			});
		});

		clearInterval(interval)
	}, 100)

	// }
});

module.exports = ActiveState;