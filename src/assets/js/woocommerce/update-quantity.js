const UpdateQuantity = jQuery(document).ready(function($) {
    var count = 0;

    $('.woocommerce').on('change', 'input.qty', function() {
        $("[name='update_cart']").trigger("click");
    })

    $('.plus').on('click', function (e) {
        e.preventDefault();

        let quantity = $(this).parent().find('.qty');
        quantity.val(parseInt(quantity.val()) + 1).change();

    })

    $('.minus').on('click', function (e) {
        e.preventDefault();

        let quantity = $(this).parent().find('.qty');
        quantity.val(parseInt(quantity.val()) - 1).change();

    })

    let interval = setInterval(function () {
        $("[name='update_cart']").removeAttr('disabled');
        count ++;
        if (count > 20) clearInterval(interval);
    }, 100)

});

module.exports = UpdateQuantity;