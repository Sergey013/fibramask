const ChangeColor = jQuery(document).ready(function($) {
    var interval;
    var color;

    $('.color-variation span').on('click', function (e) {
        $(this).parent().find('span').removeClass('selected');
        color = $(this).data('variant');

        $("#color").val($(this).data('color'));
        $("#color").change();

        $('.color-variation span').each(function () {
            $(this).removeClass('selected');

            if ($(this).attr("class").indexOf('selected') >= 0) {
                interval = setInterval(function () {
                    $('.flex-control-nav img').each(function () {
                        if ($(this).attr('src').indexOf(color) < 0) {
                            $(this).parent().hide();
                        } else {
                            $(this).parent().show();
                        }
                    })
                    clearInterval(interval)
                }, 100)
            }
        })

        $(this).addClass('selected');
    })

    $('.color-variation span').each(function () {
        $(this).removeClass('selected');
    })

    let interval_load = setInterval(function () {
        if($('.flex-active-slide img').length > 0) {
            if ($('.flex-active-slide img').attr('src').indexOf('black') > 0) {
                $('.color-variation span[data-variant="black"]').addClass('selected')
                color = 'black'
            } else if ($('.flex-active-slide img').attr('src').indexOf('grey') > 0) {
                $('.color-variation span[data-variant="grey"]').addClass('selected')
                color = 'grey'
            } else if ($('.flex-active-slide img').attr('src').indexOf('pink') > 0) {
                $('.color-variation span[data-variant="pink"]').addClass('selected')
                color = 'pink'
            } else if ($('.flex-active-slide img').attr('src').indexOf('blue') > 0) {
                $('.color-variation span[data-variant="blue"]').addClass('selected')
                color = 'blue'
            }

            $('.flex-control-nav img').each(function () {
                if ($(this).attr('src').indexOf(color) < 0) {
                    $(this).parent().hide();
                } else {
                    $(this).parent().show();
                }
            })
        }

        clearInterval(interval_load)
    }, 500)


});

module.exports = ChangeColor;