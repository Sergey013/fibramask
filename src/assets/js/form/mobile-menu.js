const MobileMenu = jQuery(document).ready(function($) {
    $('.close-btn').click(function (e) {
        e.preventDefault();

        $('.mobile-menu').fadeOut();
    });

    $('.mobile-icon').click(function (e) {
        e.preventDefault();
        $('.mobile-menu').fadeIn();
    });

    $('.ready-to-order .continue').on('click', function (e) {


        $('body, html').animate({scrollTop: '780px'})
    });

    if ($(document).width() > 600) {
        $('.button-container').remove()
        $('#wc-stripe-payment-request-wrapper').remove()
        $('#wc-stripe-payment-request-button-separator').remove()
    }
});

module.exports = MobileMenu;