const ChangeQuantity = jQuery(document).ready(function($) {
    $(".order-product .quantity").each(function() {
        $(this).find('.plus').click(function (e) {
            e.preventDefault();

            if (parseInt($(this).parent().find('input').val()) + 1 > $(this).data("max")) return;

            $(this).parent().find('input').val(parseInt($(this).parent().find('input').val()) + 1);
        });

        $(this).find('.minus').click(function (e) {
            e.preventDefault();
            if (parseInt($(this).parent().find('input').val()) - 1 <= 0) return;

            $(this).parent().find('input').val(parseInt($(this).parent().find('input').val()) - 1);
        });
    });

    $('form[name="return"] input[type="checkbox"]').change(function () {
        let count = 0;
        $('form[name="return"] input[type="checkbox"]').each(function() {
            if ($(this).prop( "checked")) {
                count ++;
                $('form[name="return"] input[type="submit"]').removeClass('disabled');
            }

            if (count <= 0) {
                $('form[name="return"] input[type="submit"]').addClass('disabled');
            }
        });
    });
});

module.exports = ChangeQuantity;