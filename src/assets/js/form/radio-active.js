const RadioActive = jQuery(document).ready(function($) {
    $('#customer_details #shipping_method input[type="radio"]').each(function() {
        if ($(this).prop('checked')) {
            $(this).parent().addClass('active');
        }
    });

    $('#customer_details #shipping_method input[type="radio"]').change(function () {
        $('#customer_details #shipping_method li').removeClass('active');

        $('#customer_details #shipping_method input[type="radio"]').each(function() {
            if ($(this).prop('checked')) {
                $(this).parent().addClass('active');
            }
        });
    });

    $('#payment input[type="submit"]').show();
    $('#payment .button-container2 .btn').hide();
    $('#payment #wc-stripe-payment-request-wrapper').css({'display': 'none!important'});

    $(document).ajaxComplete(function(e) {
        $('#payment input[type="radio"]').each(function() {
            if ($(this).prop('checked')) {
                if ($(this).val() == 'apple' || $(this).val() == 'google') {
                    $('#payment input[type="submit"]').hide();
                    $('#payment .button-container2 .btn').show();
                    $('#payment #wc-stripe-payment-request-wrapper').css({'display': 'block!important'});
                }else{
                    $('#payment input[type="submit"]').show();
                    $('#payment .button-container2 .btn').hide();
                    $('#payment #wc-stripe-payment-request-wrapper').css({'display': 'none!important'});
                }
                $(this).parent().addClass('active');
            }
        });

        $('#payment input[type="radio"]').change(function () {
            $('#payment li').removeClass('active');

            $('#payment input[type="radio"]').each(function() {
                if ($(this).prop('checked')) {

                    if ($(this).val() == 'apple' || $(this).val() == 'google') {
                        $('#payment button[type="submit"]').hide();
                        $('#payment .button-container2 .btn').show();
                        $('#payment #wc-stripe-payment-request-wrapper').css({'display': 'block!important'});
                    }else{
                        $('#payment button[type="submit"]').show();
                        $('#payment .button-container2 .btn').hide();
                        $('#payment #wc-stripe-payment-request-wrapper').css({'display': 'none!important'});
                    }
                    $(this).parent().addClass('active');
                }
            });
        });
    });


});

module.exports = RadioActive;