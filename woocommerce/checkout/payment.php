<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.3
 */

defined( 'ABSPATH' ) || exit;

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
    <div id="payment" class="woocommerce-checkout-payment">
		<?php if ( WC()->cart->needs_payment() ) : ?>
            <h3 id="payment_heading"><?php esc_html_e( 'Payment Method', 'woocommerce' ); ?></h3>
            <ul class="wc_payment_methods payment_methods methods">
				<?php if (stristr( $_SERVER['HTTP_USER_AGENT'], 'android')): ?>
                    <li class="wc_payment_method payment_method_google container">
                        <input id="payment_method_google" type="radio" class="input-radio" name="payment_method"
                               value="google">
                        <span class="checkmark">
                        </span>
                        <label for="payment_method_google">Google pay</label>
                    </li>
				<?php elseif ( stristr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || stristr( $_SERVER['HTTP_USER_AGENT'], 'ipad' ) ): ?>
                    <li class="wc_payment_method payment_method_apple container">
                        <input id="payment_method_apple" type="radio" class="input-radio" name="payment_method"
                               value="apple">
                        <span class="checkmark">
                        </span>
                        <label for="payment_method_apple">Apple pay</label>
                    </li>
				<?php endif; ?>


				<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}
				} else {
					echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
				}
				?>

            </ul>
		<?php endif; ?>
        <div class="form-row place-order">

            <noscript>
				<?php
				/* translators: $1 and $2 opening and closing emphasis tags respectively */
				printf( esc_html__( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate Totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ), '<em>', '</em>' );
				?>
                <br/>


                <button type="submit" class="button alt" name="woocommerce_checkout_update_totals"
                        value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
            </noscript>

			<?php wc_get_template( 'checkout/terms.php' ); ?>

			<?php do_action( 'woocommerce_review_order_before_submit' ); ?>


            <table class="shop_table woocommerce-checkout-review-order-table">
                <thead>
                <tr>
                    <th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
                    <th class="product-total"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				do_action( 'woocommerce_review_order_before_cart_contents' );

				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						?>
                        <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                            <td class="product-name">
								<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
								<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
								<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                            </td>
                            <td class="product-total">
								<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                            </td>
                        </tr>
						<?php
					}
				}

				do_action( 'woocommerce_review_order_after_cart_contents' );
				?>
                </tbody>
                <tfoot>

                <tr class="cart-subtotal">
                    <th><?php esc_html_e( 'Sub-total', 'woocommerce' ); ?></th>
                    <td><?php wc_cart_totals_subtotal_html(); ?></td>
                </tr>

                <tr class="cart-delivery">
                    <th><?php esc_html_e( 'Delivery', 'woocommerce' ); ?></th>
                    <td><?php $current_shipping_cost = WC()->cart->get_cart_shipping_total();
						echo $current_shipping_cost; ?> </td>
                </tr>

				<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                    <tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                        <th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
                        <td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
                    </tr>
				<?php endforeach; ?>

				<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
                    <tr class="fee">
                        <th><?php echo esc_html( $fee->name ); ?></th>
                        <td><?php wc_cart_totals_fee_html( $fee ); ?></td>
                    </tr>
				<?php endforeach; ?>

				<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
					<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
						<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited ?>
                            <tr class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                                <th><?php echo esc_html( $tax->label ); ?></th>
                                <td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
                            </tr>
						<?php endforeach; ?>
					<?php else : ?>
                        <tr class="tax-total">
                            <th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
                            <td><?php wc_cart_totals_taxes_total_html(); ?></td>
                        </tr>
					<?php endif; ?>
				<?php endif; ?>

				<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

                <tr class="order-total">
                    <th><?php esc_html_e( 'Total to pay', 'woocommerce' ); ?></th>
                    <td><?php wc_cart_totals_order_total_html(); ?></td>
                </tr>

				<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

                </tfoot>
            </table>

			<?php if (stristr( $_SERVER['HTTP_USER_AGENT'], 'android') || stristr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || stristr( $_SERVER['HTTP_USER_AGENT'], 'ipad' )): ?>
            <div class="button-container2">
				<?php if ( stristr( $_SERVER['HTTP_USER_AGENT'], 'android' ) ): ?>
                    <a href="#" class="btn google"></a>
				<?php elseif ( stristr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iphone' ) || stristr( $_SERVER['HTTP_USER_AGENT'], 'ipad' ) ): ?>
                    <a href="#" class="btn apple"></a>
				<?php endif; ?>
                <div id="wc-stripe-payment-request-wrapper" style="clear:both;padding-top:1.5em;display:none;">
                    <div id="wc-stripe-payment-request-button">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                </div>
				<?php endif; ?>
				<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>
            </div>
			<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

			<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>


            <p id="privacy-link">
                By placing the order you accept our
                <a href="<?php echo get_permalink( 41 ) ?>">Privacy Policy</a>
                and
                <a href="<?php echo get_permalink( 37 ) ?>">Return Policy.</a></p>
        </div>
    </div>
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
