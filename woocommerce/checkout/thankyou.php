<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
            <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>"
               class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
			<?php if ( is_user_logged_in() ) : ?>
                <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>"
                   class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
			<?php endif; ?>
        </p>

	<?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you for your order', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

        <h4 class="order-title">
			<?php esc_html_e( 'Order Details', 'woocommerce' ); ?>
        </h4>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                        <li class="woocommerce-order-overview__order order">
							<?php esc_html_e( 'Order number', 'woocommerce' ); ?>
                            <strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                        </li>

                        <li class="woocommerce-order-overview__date date">
							<?php esc_html_e( 'Date', 'woocommerce' ); ?>
                            <strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                        </li>

                        <li class="woocommerce-order-overview__total total">
							<?php esc_html_e( 'Delivery Address', 'woocommerce' ); ?>
                            <strong><?php
								echo $order->get_formatted_shipping_address();
								?></strong>
                        </li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="woocommerce-order">
                    <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

						<?php if ($order->get_billing_email() ) : ?>
                            <li class="woocommerce-order-overview__email email">
								<?php esc_html_e( 'Email Address', 'woocommerce' ); ?>
                                <strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                            </li>
						<?php endif; ?>

                        <!--                        <li class="woocommerce-order-overview__total total">-->
                        <!--							--><?php //esc_html_e( 'Phone number:', 'woocommerce' ); ?>
                        <!--                            <strong>-->
						<?php //echo $order->get_billing_phone(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?><!--</strong>-->
                        <!--                        </li>-->

						<?php if ( $order->get_payment_method_title() ) : ?>
                            <li class="woocommerce-order-overview__payment-method method">
								<?php esc_html_e( 'Payment method', 'woocommerce' ); ?>
                                <strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
                            </li>
						<?php endif; ?>


                        <li class="woocommerce-order-overview__total total">
							<?php esc_html_e( 'Delivery Option', 'woocommerce' ); ?>
                            <strong><?php echo $order->get_shipping_method(); ?></strong>
                        </li>

                    </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="cart-block">
        <h4 class="order-title">
			<?php esc_html_e( 'Products', 'woocommerce' ); ?>
        </h4>
        <div class="cart-items">
	<?php endif; ?>
		<?php

		$products = $order->get_items();

		foreach ( $order->get_items() as $item_id => $item_values ) {
			$product_id = $item_values->get_product_id();
			$_product   = wc_get_product( $item_values->get_variation_id() );


			$rules = get_field( 'rules', $item_values->get_product_id() );
			foreach ( $rules as $rule ) {
				if ( $item_values->get_quantity() >= $rule['quantity'] ) {
					$total = strip_tags( wc_price( $item_values->get_quantity() * $rule['piece_price'] ) );
				}
			}

			?>
            <div class="item">
                <div class="picture">
					<?php echo $_product->get_image(); ?>
                </div>
                <div class="info">
					<?php $name = explode( '-', $_product->get_name() ); ?>
                    <h5><?php echo $name[0]; ?></h5>
                    <h6>The Original</h6>
                    <span class="gray"><?php echo $item_values->get_meta( 'color' ); ?>
                        | <?php echo $item_values->get_meta( 'size' ); ?></span>
                </div>
                <div class="price">
					<?php echo $total; ?>
                </div>
            </div>
		<?php } ?>
        </div>
        </div>

        <table class="shop_table woocommerce-checkout-review-order-table">
            <thead>
            <tr>
                <th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
                <th class="product-total"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>

            <tr class="cart-subtotal">
                <th><?php esc_html_e( 'Sub-total', 'woocommerce' ); ?></th>
                <td><span class="woocommerce-Price-amount amount"><span
                                class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol() ?></span><?php echo $order->get_subtotal() ?></span>
                </td>
            </tr>

            <tr class="cart-delivery">
                <th><?php esc_html_e( 'Delivery', 'woocommerce' ); ?></th>
                <td><span class="woocommerce-Price-amount amount"><span
                                class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol() ?></span><?php echo $order->get_shipping_total() ?> </span>
                </td>
            </tr>


            <tr class="order-total">

                <th><?php esc_html_e( 'Total Paid', 'woocommerce' ); ?></th>
                <td> <span class="woocommerce-Price-amount amount"><span
                                class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol() ?></span><?php echo $order->get_total() ?> </span>
                </td>
            </tr>


            </tfoot>
        </table>

        <a href="<?php echo  get_permalink(78)?>" class="button btn">Any questions?</a>


	<?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>


</div>
