<?php
    if (! defined('ABSPATH')) {
        exit; // Exit if accessed directly.
    }

if (isset($_POST['stripeToken'])) { $token = $_POST['stripeToken'];}
?>
<?php if (stristr($_SERVER['HTTP_USER_AGENT'],'android')): ?>
    <a href="#" class="btn google"></a>
    <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button button alt wc-forward">
        <?php esc_html_e('Go to Checkout', 'woocommerce'); ?>
    </a>
<?php elseif (stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'iphone') || stristr($_SERVER['HTTP_USER_AGENT'],'ipad')): ?>
    <a href="#" class="btn apple"></a>
    <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button button alt wc-forward">
        <?php esc_html_e('Go to Checkout', 'woocommerce'); ?>
    </a>
<?php else: ?>
    <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button primary button alt wc-forward">
        <?php esc_html_e('Go to Checkout', 'woocommerce'); ?>
    </a>
<?php endif; ?>
</div>
