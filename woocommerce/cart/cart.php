<?php

defined( 'ABSPATH' ) || exit;

function getDiscount( $count ) {
	switch ( $count ) {
		case $count >= 3 && $count < 4:
			return '<span class="discount">10% off</span>';
		case $count >= 4 && $count < 5:
			return '<span class="discount">15% off</span>';
		case $count >= 5 :
			return '<span class="discount">20% off</span>';
	}
}

do_action( 'woocommerce_before_cart' ); ?>
<div class="page-wrapper">
    <div class="cart-block">
        <h2><?php echo get_the_title(); ?></h2>

        <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
            <div class="cart-items">
				<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ): ?>
					<?php do_action( 'woocommerce_before_cart_contents' ); ?>
					<?php
					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
					?>
					<?php if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ): ?>
						<?php $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key ); ?>
                        <div class="item">
                            <div class="picture">
								<?php echo $_product->get_image(); ?>
                            </div>
                            <div class="info">
								<?php $name = explode( '-', $_product->get_name() ); ?>
                                <h5><?php echo $name[0]; ?></h5>
                                <h6>The Original</h6>
                                <span>
                                    <?php echo $name[1]; ?>
                                    | <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
                                </span>

                                <div class="quantity-container"
                                     data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
									<?php if ( $_product->is_sold_individually() ): ?>
										<?php $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key ); ?>
									<?php else: ?>
										<?php
										$product_quantity = woocommerce_quantity_input(
											array(
												'input_name'   => "cart[{$cart_item_key}][qty]",
												'input_value'  => $cart_item['quantity'],
												'max_value'    => $_product->get_max_purchase_quantity(),
												'min_value'    => '0',
												'product_name' => $_product->get_name(),
											),
											$_product,
											false
										);
										?>
									<?php endif; ?>

                                    <a href="" class="minus"></a>
									<?php echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); ?>
                                    <a href="" class="plus"></a>

                                </div>

                            </div>
                            <div class="price">
								<?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
								<?php echo getDiscount( $cart_item['quantity'] ) ?>
                            </div>
                        </div>
					<?php endif; ?>
				<?php endforeach; ?>
                <button type="submit" class="button" name="update_cart"
                        value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

				<?php do_action( 'woocommerce_cart_actions' ); ?>

				<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
            </div>
            <div class="cart-totals">
				<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

                <div class="cart-collaterals">
					<?php do_action( 'woocommerce_cart_collaterals' ); ?>
                </div>

				<?php do_action( 'woocommerce_after_cart' ); ?>
            </div>
			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
        </form>
    </div>
</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
