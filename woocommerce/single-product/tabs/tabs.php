<?php

if (!defined('ABSPATH')) exit;
global $product;
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );
$items = get_field('faq', $product->post->ID);
if (!empty( $product_tabs)): ?>

    <div class="woocommerce-tabs wc-tabs-wrapper">
            <div class="product-description">
                <div class="page-wrapper">
                    <div class="description-block">
                        <div class="description-text">
                            <h2><?php echo get_field('title', $product->post->ID); ?></h2>
                            <?php echo get_field('text', $product->post->ID); ?>
                        </div>
                        <div class="description-faq">
                            <h2>Frequently Asked Questions</h2>

                            <div class="accordition-block">
                                <div class="accordition-block-wrapper">
                                    <?php foreach ($items as $index=>$item): ?>
                                        <div class="item toggle <?php if($index == 0): echo 'opened active'; endif; ?>">
                                            <span class="content-wrap">
                                                <h5><?php echo $item->post_title; ?></h5>
                                                <div class="<?php if($index == 0): echo 'show'; endif; ?>">
                                                    <?php echo $item->post_content; ?>
                                                </div>
                                            </span>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php do_action( 'woocommerce_product_after_tabs' ); ?>
    </div>

<?php endif; ?>