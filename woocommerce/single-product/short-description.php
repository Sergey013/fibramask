<?php
    if (! defined('ABSPATH')) exit;
    global $product;
?>

<div class="short-description">
    <div class="product-title-block">
        <div class="product-title">
            <h1><?php echo $product->post->post_title; ?></h1>
            <span>The Original</span>
        </div>
        <div class="product-price">
            <?php echo $product->get_price_html(); ?>
            <span class="exert"><?php echo get_field('special', $product->post->ID); ?></span>
        </div>
    </div>
</div>