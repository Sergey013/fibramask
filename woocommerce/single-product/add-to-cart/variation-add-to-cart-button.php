<?php
    defined('ABSPATH') || exit;
    global $product;
    $price_variants = get_field('rules', $product->post->ID);
?>

<div class="select-quantity">
    <span class="dynamic-price" id="dynamic-price">
        <select name="dynamic-price">
            <?php foreach ($price_variants as $price): ?>
                <option value="<?php echo $price['quantity']; ?>" price="<?php echo strip_tags(($price['piece_price'])); ?>" piece="<?php echo strip_tags(wc_price($price['piece_price'])); ?>" discount="<?php if($price['discount'] != 0): echo $price['discount'].'% off'; endif; ?>"><?php echo $price['quantity']?></option>
            <?php endforeach; ?>
        </select>
    </span>


    <?php echo wpautop(get_field('excerpt', $product->post->ID)); ?>
    <?php do_action('woocommerce_before_add_to_cart_button' ); ?>
    <?php
        do_action( 'woocommerce_before_add_to_cart_quantity' );

        woocommerce_quantity_input(
            array(
                'min_value'   => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                'max_value'   => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                'input_value' => isset($_POST['quantity']) ? wc_stock_amount(wp_unslash($_POST['quantity'])) : $product->get_min_purchase_quantity(),
            )
        );

        do_action('woocommerce_after_add_to_cart_quantity');
    ?>

    <button type="submit" class="single_add_to_cart_button button alt">Add to Cart</button>

    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

    <input type="hidden" name="add-to-cart" value="<?php echo absint($product->get_id()); ?>" />
    <input type="hidden" name="product_id" value="<?php echo absint($product->get_id()); ?>" />
    <input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>