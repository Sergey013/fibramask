<?php
defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys  = array_keys($attributes);
$variations_json = wp_json_encode($available_variations);
$variations_attr = function_exists('wc_esc_json') ? wc_esc_json($variations_json) : _wp_specialchars($variations_json, ENT_QUOTES, 'UTF-8', true);

do_action('woocommerce_before_add_to_cart_form');

?>

<form class="variations_form cart" action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint($product->get_id()); ?>" data-product_variations="<?php echo $variations_attr; ?>">
    <?php do_action('woocommerce_before_variations_form'); ?>

    <div class="variations-list">
        <?php foreach ($attributes as $attribute_name => $options): ?>
            <?php if($attribute_name == 'Color'): ?>
                <div class="color-variation">
                    <?foreach ($options as $index=>$opt): ?>
                        <span data-color="<?php echo $opt; ?>" data-variant="<?php echo strtolower(explode(' ', $opt)[1]); ?>" class="<?php echo strtolower(explode(' ', $opt)[1]); ?> <?php if($index == 0): echo 'selected'; endif; ?>">
                            <strong></strong>
                        </span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if($attribute_name == 'Size'): ?>
                <span class="sizes" id="select-size">
                    <select name="select-size">
                        <?foreach ($options as $index=>$opt): ?>
                            <option value=""><?php echo $opt; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <table class="variations" cellspacing="0">
        <tbody>
        <?php foreach ( $attributes as $attribute_name => $options ) : ?>
            <tr>
                <td class="label"><label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></label></td>
                <td class="value">
                    <?php
                    wc_dropdown_variation_attribute_options(
                        array(
                            'options'   => $options,
                            'attribute' => $attribute_name,
                            'product'   => $product,
                        )
                    );
                    echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php
    /**
     * Hook: woocommerce_before_single_variation.
     */
    do_action( 'woocommerce_before_single_variation' );

    /**
     * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
     *
     * @since 2.4.0
     * @hooked woocommerce_single_variation - 10 Empty div for variation data.
     * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
     */
    do_action( 'woocommerce_single_variation' );

    /**
     * Hook: woocommerce_after_single_variation.
     */
    do_action( 'woocommerce_after_single_variation' );
    ?>

    <?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>
<?php
do_action( 'woocommerce_after_add_to_cart_form' );
