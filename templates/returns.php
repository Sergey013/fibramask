<?php /* Template Name: Returns */ ?>

<?php
$message       = '';
$success       = false;
$emailError    = false;
$orderError    = false;
$refundSuccess = false;
$total_blocks  = 0;

if ( ! empty( $_POST['order_id'] ) ) {
	$order = wc_get_order( $_POST['order_id'] );
	$order->update_status( 'wc-requested-refund' );
	$success       = true;
	$refundSuccess = false;


    $order_items = $order->get_items();
    $refund_amount = 0;
    $line_items = array();
    $counter = 1;

    foreach($order_items as $item_id => $item) {
        $item_meta = $order->get_item_meta($item_id);
        $tax_data = $item_meta['_line_tax_data'];

        $refund_tax = 0;

        if (is_array($tax_data[0])) {
            $refund_tax = array_map('wc_format_decimal', $tax_data[0]);
        }

        $refund_amount = wc_format_decimal($refund_amount) + wc_format_decimal($item_meta['_line_total'][0]);

        $rules = get_field('rules', $item->get_product_id());

        foreach ($rules as $rule) {
            if ($_POST['quantity-' . $counter . ''] >= $rule['quantity']) {
                $refund_amount = $_POST['quantity-' . $counter . ''] * wc_format_decimal($rule['piece_price']);
            }
        }

	$refundSuccess = true;

	$order->add_meta_data( 'refund_count', 2 );

        if (!empty($_POST['return-' . $counter . '']) && $_POST['return-' . $counter . ''] == 'on') {
            $line_items[$item_id] = array(
                'qty' => $_POST['quantity-' . $counter . ''],
                'refund_total' => $refund_amount,
                'refund_tax' => $refund_tax);
        }

        $counter += 1;
    }


    if (!$refundSuccess) {
        $refund = wc_create_refund(array(
            'amount' => $refund_amount,
            'reason' => '',
            'order_id' => $_POST['order_id'],
            'line_items' => $line_items,
            'refund_payment' => true
        ));
    }

    if (empty($refund->errors)) {
        $refundSuccess = true;
    }
}
if(empty( $_POST['order-number'] ) && empty($_POST['email'] ) && ! empty( $_POST)) {
    $orderError = true;
    $emailError = true;
    $orderError = true;
    $message    = 'All fields are required';
}
if ( ! empty( $_POST['order-number'] ) ) {
	if ( empty( $_POST['email'] ) || ! filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL ) ) {
		$emailError = true;
	}

	if ( empty( $_POST['order-number'] ) || ! filter_var( $_POST['order-number'], FILTER_VALIDATE_INT ) ) {
		$orderError = true;
	}



	if ( ! wc_get_order( $_POST['order-number'] ) ) {
		$orderError = true;
		$message    = 'This order could not be found.';
	}

	$email        = $_POST['email'];
	$order_number = $_POST['order-number'];


	$order = wc_get_order( $order_number );
	if ( $order !== false ) {
		if ( $order->get_status() == 'refunded' || $order->get_status() == 'requested-refund' ) {
			$orderError = true;
			$message    = 'Order has been already refunded';
			$success    = false;
		}
	}

	if ( ! $orderError && ! $emailError ) {

		$order_data = $order->get_data();
		$items      = $order->get_items();

		if ( $order_data['billing']['email'] !== $email ) {
			$message = 'This order could not be found.';
			$success = false;
		} else {
			$error   = '';
			$message = 'Kindly find the status of your order below.';
			$success = true;
		}
	}


}

?>

<?php get_header(); ?>
    <section id="content">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-content">
                    <div class="page-wrapper">
                        <div class="returns-block">
                            <h2><?php the_title(); ?></h2>
                            <span>
                                <?php if ( ! $success && ! $refundSuccess ): ?>
                                    <div class="text">
                                        You can use the form below to start the return process for your FibraMask®.<br>
                                        Kindly read our <a href="/return-policy/">Returns Policy</a> for more information.
                                    </div>
                                <?php elseif ( $success && ! $refundSuccess ): ?>
                                    <div class="info-order">
                                        <span>Order Number</span>
                                        <strong><?php echo $order_number; ?></strong>
                                    </div>
                                    <div class="message">
                                        Kindly note that FibraMask® products can only be returned in their<br>
                                        original, vacuum-sealed packaging. See our <a href="/return-policy/">Returns Policy</a> for more.
                                    </div>
                                <?php else: ?>
                                    <div class="info-order">
                                        <span>Order Number</span>
                                        <strong><?php echo $_POST['order_id']; ?></strong>
                                    </div>
                                    <div class="message">
                                        Kindly note that FibraMask® products can only be returned in their<br>
                                        original, vacuum-sealed packaging. See our <a href="/return-policy/">Returns Policy</a> for more.
                                    </div>
                                <?php endif; ?>
                            </span>
							<?php if ( ! $success && ! empty( $message ) ): ?>
                                <div class="message">
									<?php echo $message; ?>
                                </div>
							<?php endif; ?>

							<?php if ( ! $success && ! $refundSuccess ): ?>
                                <form name="find" enctype="multipart/form-data"
                                      action="<?php echo get_page_link( get_the_ID() ); ?>" method="post">
                                    <label>
                                        Email Address
                                        <input type="text" class="<?php if ( $emailError ): echo 'err'; endif; ?>"
                                               name="email" value="" size="40" aria-required="true"
                                               aria-invalid="false">
                                    </label>
                                    <label>
                                        Order Number
                                        <input type="text" class="<?php if ( $orderError ): echo 'err'; endif; ?>"
                                               name="order-number" value="" size="40" aria-required="true"
                                               aria-invalid="false">
                                    </label>
                                    <input type="submit" value="Find Order">
                                </form>
							<?php elseif ( $success && ! $refundSuccess ): ?>
                                <h3>Select Item to Return</h3>
                                <form name="return" enctype="multipart/form-data"
                                      action="<?php echo get_page_link( get_the_ID() ); ?>" method="post">
                                    <input type="hidden" name="order_id" value="<?php echo $order_number; ?>">
									<?php $count = 0;
									foreach ( $items as $item ): ?>
										<?php
										$rules = get_field( 'rules', $item->get_product_id() );
										foreach ( $rules as $rule ) {
											if ( $item->get_quantity() >= $rule['quantity'] ) {
												$total = strip_tags( wc_price( $item->get_quantity() * $rule['piece_price'] ) );
											}
										}

										$refunded = $order->get_qty_refunded_for_item( $item->get_id() );

										$variation        = wc_get_product( $item->get_variation_id() );
										$updated_image_id = $variation->get_image_id();

										$count += 1;
										?>
										<?php if ( $item->get_quantity() + $refunded > 0 ): $total_blocks ++; ?>
                                            <div class="order-product">
                                                <div class="product-picture"
                                                     style="background: #f8f8f8 url(<?php echo wp_get_attachment_image_url( $updated_image_id ) ?>) center no-repeat"></div>
                                                <div class="product-info">
                                                    <div class="content">
                                                        <div class="info">
                                                            <h5><?php echo wc_get_product( $item->get_product_id() )->get_title(); ?></h5>
                                                            <span>The Original</span>
                                                            <span class="gray"><?php echo $item->get_meta( 'color' ); ?>
                                                                | <?php echo $item->get_meta( 'size' ); ?></span>
                                                        </div>
                                                        <div class="price">
                                                            <h5><?php echo $total; ?></h5>
                                                        </div>
                                                    </div>
                                                    <div class="product-form">
                                                        <div class="quantity">
                                                            <a href="" class="minus"></a>
                                                            <input type="text" name="quantity-<?php echo $count; ?>"
                                                                   value="<?php echo $item->get_quantity() + $refunded; ?>">
                                                            <a href="" class="plus"
                                                               data-max="<?php echo $item->get_quantity() + $refunded; ?>"></a>
                                                        </div>
                                                        <label class="container">
                                                            <input type="checkbox" name="return-<?php echo $count; ?>">
                                                            <span class="checkmark">
                                                            </span>
                                                            Return this item
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php if ( $total_blocks > 0 ): ?>
                                        <input type="submit" class="disabled" value="Start Return Process">
									<?php else: ?>
                                        <h4>Sorry all items was refunded!</h4>
									<?php endif; ?>
                                </form>
							<?php else: ?>
                                <p>If you have any general questions, feedback or inquires, please use the form below to
                                    contact FibraMask®.</p>
                                <div class="address">
                                    FibraMask Lda<br>
                                    Av. D. João II nº<br>
                                    75 4715 - 303 Braga <br>
                                    Portugal
                                </div>
                                <p>Once received, your return will be verified and your payment (excl. shipping costs)
                                    will be refunded to the same payment method used in the original order (i.e. card or
                                    PayPal).</p>
                                <p>The entire process may take 10-14 days.</p>
                                <p></p>
                                <a href="/support/" class="btn">Need More Help?</a>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		<?php endwhile; ?>
    </section>
<?php echo entry_footer(); ?>
<?php get_footer();