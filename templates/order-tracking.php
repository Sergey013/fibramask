<?php /* Template Name: Order tracking */ ?>

<?php
$message    = '';
$success    = false;
$emailError = false;
$orderError = false;


if ( ! empty( $_POST ) ) {
	if ( empty( $_POST['email'] ) || ! filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL ) ) {
		$emailError = true;
	}

	if ( empty( $_POST['order-number'] ) || ! filter_var( $_POST['order-number'], FILTER_VALIDATE_INT ) ) {
		$orderError = true;
	}

	if ( ! wc_get_order( $_POST['order-number'] ) ) {
		$orderError = true;
		$message    = 'This order could not be found.';
	}


	if ( empty( $_POST['order-number'] ) && empty( $_POST['email'] ) && ! empty( $_POST ) ) {
		$orderError = true;
		$emailError = true;
		$orderError = true;
		$message    = 'All fields are required';
	}

	$email        = $_POST['email'];
	$order_number = $_POST['order-number'];

	if ( ! $orderError && ! $emailError ) {
		$order      = wc_get_order( $order_number );
		$order_data = $order->get_data();

		if ( $order_data['billing']['email'] !== $email ) {
			$message = 'This order could not be found.';
			$success = false;
		} else {
			$error            = '';
			$order_date       = $order_data['date_created']->date( 'd F Y' );
			$delivery_address = $order_data['billing']['address_1'] . ', ' . $order_data['billing']['city'] . ' ' . $order_data['billing']['country'];
			$delivery_option  = $order->get_shipping_method();
			$order_status     = get_post_meta( $order->get_id(), 'order_tracking', true );
			$message          = 'Kindly find the status of your order below.';
			$success          = true;
		}
	}
}
?>

<?php get_header(); ?>
    <section id="content">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-content">
                    <div class="page-wrapper">
                        <div class="order-tracking-block">
                            <h2><?php the_title(); ?></h2>
                            <span>
                                <?php if ( ! $success ): ?>
	                                <?php echo get_field( 'text', get_the_ID() ); ?>
                                <?php else: ?>
	                                <?php echo $message; ?>
                                <?php endif; ?>
                            </span>
							<?php if ( ! $success && ! empty( $message ) ): ?>
                                <div class="message">
									<?php echo $message; ?>
                                </div>
							<?php endif; ?>
							<?php if ( ! $success ): ?>
                                <form action="<?php echo get_page_link( get_the_ID() ); ?>" method="post">
                                    <label>
                                        Email Address
                                        <input type="text" class="<?php if ( $emailError ): echo 'err'; endif; ?>"
                                               name="email" value="" size="40" aria-required="true"
                                               aria-invalid="false">
                                    </label>
                                    <label>
                                        Order Number
                                        <input type="text" class="<?php if ( $orderError ): echo 'err'; endif; ?>"
                                               name="order-number" value="" size="40" aria-required="true"
                                               aria-invalid="false">
                                    </label>
                                    <input type="submit" value="Get Order Status">
                                </form>
							<?php else: ?>
                                <div class="order-status <?php echo $order_status; ?>">
                                    <div class="status-item"></div>
                                    <div class="line"></div>
                                    <div class="status-item"></div>
                                    <div class="line"></div>
                                    <div class="status-item"></div>
                                </div>
                                <div class="info-order">
                                    <span>Order Number</span>
                                    <strong><?php echo $order_number; ?></strong>
                                    <span>Date</span>
                                    <strong><?php echo $order_date; ?></strong>
                                    <span>Delivery Address</span>
                                    <strong><?php echo $delivery_address; ?></strong>
                                    <span>Delivery Option</span>
                                    <strong><?php echo $delivery_option; ?></strong>
                                </div>
                                <a href="<?php echo get_page_link( get_the_ID() ); ?>" class="btn">Find Another
                                    Order</a>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		<?php endwhile; ?>
    </section>
<?php echo entry_footer(); ?>
<?php get_footer();