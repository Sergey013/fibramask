	</div>

	<footer class="main-footer">
		<div class="site-wrapper">
            <div class="footer-logo ">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri();?>/dist/assets/images/footer-logo.svg">
                </a>
            </div>
            <div class="footer-bar">
                <div class="copyright">
                    &copy; Copyright <?php echo date('Y'); ?> by <?php echo get_bloginfo( 'name' ); ?>™.  <?php if(wp_is_mobile()): echo '<br>'; endif; ?>All rights reserved. Made with<img src="<?php echo get_template_directory_uri();?>/dist/assets/images/heart.png">in Braga.
                </div>
                <div class="footer-menu">
                    <div class="social">
                        <?php while( have_rows('social_icons', 'option') ): the_row(); ?>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('icon'); ?>" alt=""></a>
                        <?php endwhile; ?>
                    </div>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'menu-2',
                        'menu_id'        => 'footer-menu',
                    ));
                    ?>
                </div>
            </div>
        </div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>
