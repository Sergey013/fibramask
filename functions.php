<?php
//robin@fibramask.com
require_once 'include/helpers.php';
require_once 'include/common-settings.php';
require_once 'include/menus-register.php';
require_once 'include/shopping-cart-ajax.php';
require_once 'include/hide-menu-items.php';
require_once 'include/faq-post-type.php';
require_once 'include/recalculate-price.php';
require_once 'include/common-theme-options.php';
require_once 'include/shipping-descriptions.php';
require_once 'include/trustpilot-review-post-type.php';
require_once 'include/admin-order-tracking.php';
require_once 'include/add-to-cart-popup.php';
require_once 'include/shop-redirection.php';
require_once 'include/new-order-status.php';

function misha_remove_checkout_fields( $fields ) {

	$fields['billing_phone']['required'] = false;

	// Billing fields
	unset( $fields['billing']['billing_company'] );
	unset( $fields['billing']['billing_phone'] );
	unset( $fields['billing']['billing_state'] );

	// Shipping fields
	unset( $fields['shipping']['shipping_company'] );
	unset( $fields['shipping']['shipping_state'] );

	// Order fields
	unset( $fields['order']['order_comments'] );

    $fields['billing']['billing_country']['label'] = 'Country';
    $fields['billing']['billing_country']['priority'] = 1000;



	return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'misha_remove_checkout_fields' );

add_action( 'save_post', 'save_custom_code_after_order_details', 10, 1 );
function save_custom_code_after_order_details( $post_id ) {
    if (get_post_type($post_id) != 'shop_order') return;

    if (get_field('confirm_refund', $post_id) == 'yes') {
        //var_dump(11111111111);
    }

    //die();
}

add_filter( 'woocommerce_shipping_fields',
    'wc_npr_filter_shipping_fields', 10, 1 );
function wc_npr_filter_shipping_fields( $address_fields ) {
    $address_fields['shipping_first_name']['required'] = true;
    $address_fields['shipping_last_name']['required'] = true;
    $address_fields['shipping_address_1']['required'] = true;
    $address_fields['shipping_address_2']['required'] = false;
    $address_fields['shipping_city']['required'] = true;
    $address_fields['shipping_country']['required'] = true;
    $address_fields['shipping_postcode']['required'] = true;
    return $address_fields;
}

add_action( 'woocommerce_after_checkout_validation', 'shipping_time_optionss', 9999, 2);
function shipping_time_optionss( $fields, $errors ){
    if ( empty( $_POST['shipping_first_name'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping First name</b> is a required field.' ) );
    }
    if ( empty( $_POST['shipping_last_name'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping Last name</b> is a required field.' ) );
    }
    if ( empty( $_POST['shipping_address_1'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping Street address</b> is a required field.' ) );
    }
    if ( empty( $_POST['shipping_city'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping Town / City</b> is a required field.' ) );
    }
    if ( empty( $_POST['shipping_country'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping Country</b> is a required field.' ) );
    }
    if ( empty( $_POST['shipping_postcode'] ) ) {
        $errors->add( 'woocommerce_password_error', __( '<b>Shipping Postcode / ZIP</b> is a required field.' ) );
    }
}