<?php get_header(); ?>
    <section id="content">
        <?php while (have_posts()) : ?>
            <?php the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-content">
                    <?php the_content(); ?>
                    <?php get_template_part('template-parts/flexible-content'); ?>
                </div>
            </div>
        <?php endwhile; ?>
    </section>
    <?php echo entry_footer(); ?>
<?php get_footer();